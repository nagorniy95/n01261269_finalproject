﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRUD
{
    public partial class _Default : Page
    {
        public string my_query  = "select pageid, pagetitle as 'Page Title', pagecontent as Content, pageauthor as Author from pages";

        protected void Page_Load(object sender, EventArgs e)
        {

            debug.InnerHtml = my_query;
            

            page_select.SelectCommand = my_query;
            page_list.DataSource = Page_Bind(page_select);
            page_list.DataBind();
        }

        protected DataView Page_Bind(SqlDataSource src)
        {

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["Page Title"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";
                 


            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;

        }



        protected void Search_Page(object sender, EventArgs e)
        {
            string key = page_key.Text;

            if (key != "")
            {
                string newsql = my_query + " WHERE ";
                
                List<string> sec = new List<string>();

                newsql +=
                    " pagetitle LIKE '%" + key + "%'" +
                    " OR pageauthor LIKE '%" + key + "%' ";
                debug.InnerHtml = newsql;
               
                page_select.SelectCommand = newsql;

                //Intercept the "auto bind" and create our own binding
                page_list.DataSource = Page_Bind(page_select);

                page_list.DataBind();
            }
            

        }

    }
}