﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="CRUD.NewPage"  MasterPageFile="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h3>New Page</h3>
    <asp:sqldatasource runat="server" ID="page_select"
        connectionString="<%$Connectionstrings:final_sql_con %>">

    </asp:sqldatasource>

    <label>Page Title:</label><br />
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page name">
        </asp:RequiredFieldValidator>
    <br />

    <label>Page Content:</label><br />
        <asp:TextBox ID="page_content" runat="server" Height="100"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter your Content">
        </asp:RequiredFieldValidator>
    <br />

    <label>Author:</label><br />
        <asp:TextBox ID="page_author" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_author"
            ErrorMessage="Enter your Name:">
        </asp:RequiredFieldValidator>
    <br />

    <ASP:Button Text="Add Page" runat="server" OnClick="AddPage"/>

    <div runat="server" id="debug" class="querybox"></div>
</asp:Content>
