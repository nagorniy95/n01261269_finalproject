﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CRUD.usercontrol
{
    public partial class pagemenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            page_menu.SelectCommand = "select * from pages";
            DataView myview = (DataView)page_menu.Select(DataSourceSelectArguments.Empty);
            string menu_items = "";


            foreach (DataRowView row in myview)
            {
                string id  = row["pageid"].ToString();
                string title  = row["pagetitle"].ToString();


                menu_items += "<li>"+
                    "<a href=\"Page.aspx?pageid="
                    + id 
                    + "\">" 
                    + title
                    + "</a></li>"; 
                



            }
            menu_container.InnerHtml = menu_items;

        }
        
    }



}