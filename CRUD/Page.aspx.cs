﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CRUD
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //debug.InnerHtml = "I'm trying to get the page of id " + pageid;

            DataRowView pageview = getPageTitle(Convert.ToInt32(pageid));

            page_name.InnerHtml = pageview["pagetitle"].ToString();
            author_name.InnerHtml += pageview["pageauthor"].ToString();
            page_content.InnerHtml = pageview["pagecontent"].ToString();

        }




        protected DataRowView getPageTitle(int id)
        {

            string query = "select * from pages where pageid=" + pageid;
            page_select.SelectCommand = query;
           

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

           
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; //gets the first result which is always the student
            //string studentinfo = studentview[0][colname].ToString();
            return pagerowview;

        }

        protected void DelPage(object sender, EventArgs e)
        {
            string delquery = "DELETE FROM pages WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();

        }
    }
}