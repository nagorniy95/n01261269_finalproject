﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRUD
{
    public partial class NewPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private string addquery = "INSERT INTO Pages" +
            "(pagetitle,pagecontent,pageauthor) VALUES";

        protected void AddPage(object sender, EventArgs e)
        {
            //first get the inputs
            string title = page_title.Text.ToString();
            string content = page_content.Text.ToString();
            string author = page_author.Text.ToString();
            //teacher_pick is the DDL inside the usercontrol,
            //It's more difficult to get the value.

            //instead we can use a field on the class of
            //TeacherPick (the control), to pass along the selected ID
           
            //We implemented a sequence by setting
            //the identity in the primary key of the import scripts
            //redownload the import scripts to get it working.
            addquery += "('" +
                title + "','" + content + "','" + author + "')";

            debug.InnerHtml = addquery;

            //Finally when we've done all our trickery to make
            //the sql command work, we can now do it!
            page_select.InsertCommand = addquery;
            page_select.Insert();
        }
    }
}