﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="CRUD.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="page_name" runat="server"></h3>

    <h5 id="author_name" runat="server">Author name: </h5>

    <p runat="server" id="page_content"></p>


   <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>



    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_page_btn"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="del_debug" class="querybox" runat="server"></div>
    <a href="EditPage.aspx?Pageid=<%Response.Write(this.pageid);%>">Edit</a>

<%--    <div id="debug" runat="server"></div>--%>

</asp:Content>
