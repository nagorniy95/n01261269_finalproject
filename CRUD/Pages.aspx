﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="CRUD._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h3>Manage Pages</h3>

    <asp:TextBox runat="server" ID="page_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Page"/><br /><br />

    <asp:sqldatasource runat="server" ID="page_select"
        connectionString="<%$Connectionstrings:final_sql_con %>"></asp:sqldatasource>

    <asp:DataGrid runat="server" ID="page_list" ></asp:DataGrid>

    <div runat="server" id="debug"></div>
     <a href="NewPage.aspx">New Page</a>

</asp:Content>
