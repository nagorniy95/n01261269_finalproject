﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="CRUD.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="edit_page_title">Edit Page</h3>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">

    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <div class="inputrow">
        <label>Page title:</label>
        <asp:TextBox ID="page_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_name"
            ErrorMessage="Enter a page name...">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Author name:</label>
        <asp:TextBox ID="author_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="author_name"
            ErrorMessage="Enter an author name...">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Content:</label>
        <asp:TextBox ID="page_content" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter your new content...">
        </asp:RequiredFieldValidator>
    </div>


    <ASP:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>

    <div runat="server" id="debug" class="querybox"></div>






</asp:Content>
